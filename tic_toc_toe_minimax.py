#!/usr/bin/env python3
from math import inf as infinity
from random import choice
import time
import random
import numpy as np

RANDOM = -1
COMP = +1
board = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0],
]


def evaluate(state):
    """
    Function to heuristic evaluation of state.
    """
    if move_was_winning_move(state, COMP):
        score = +1
    elif move_was_winning_move(state, RANDOM):
        score = -1
    else:
        score = 0

    return score

def move_was_winning_move(S, p):
    if np.max((np.sum(S, axis=0)) * p) == 3:
        return True

    if np.max((np.sum(S, axis=1)) * p) == 3:
        return True

    if (np.sum(np.diag(S)) * p) == 3:
        return True

    if (np.sum(np.diag(np.rot90(S))) * p) == 3:
        return True

    return False



def game_over(state):
    """
    This function test if the random or ai wins
    """
    return move_was_winning_move(state, RANDOM) or move_was_winning_move(state, COMP)


def empty_cells(state):
    """
    Each empty cell will be added into cells' list
    """
    cells = []

    for x, row in enumerate(state):
        for y, cell in enumerate(row):
            if cell == 0:
                cells.append([x, y])

    return cells


def valid_move(x, y):
    """
    A move is valid if the chosen cell is empty
    """
    if [x, y] in empty_cells(board):
        return True
    else:
        return False


def set_move(x, y, player):
    """
    Set the move on board, if the coordinates are valid
    """
    if valid_move(x, y):
        board[x][y] = player
        return True
    else:
        return False


def minimax(state, depth, player):
    """
    AI function that choice the best move
    :param state: current state of the board
    :param depth: node index in the tree (0 <= depth <= 9),
    :param player: an random or a ai
    :return: a list with [the best row, best col, best score]
    """
    if player == COMP:
        best = [-1, -1, -infinity]
    else:
        best = [-1, -1, +infinity]

    if depth == 0 or game_over(state):
        score = evaluate(state)
        return [-1, -1, score]

    for cell in empty_cells(state):
        x, y = cell[0], cell[1]
        state[x][y] = player
        score = minimax(state, depth - 1, -player)
        state[x][y] = 0
        score[0], score[1] = x, y

        if player == COMP:
            if score[2] > best[2]:
                best = score  # max value
        else:
            if score[2] < best[2]:
                best = score  # min value

    return best


        
symbols = { 1:'x',
           -1:'o',
            0:' '}

def print_game_state(S):
    B = np.copy(S).astype(object)
    for n in [-1, 0, 1]:
        B[B==n] = symbols[n]
    print (B)
    


def ai_turn(c_choice, r_choice):
    """
    It calls the minimax function if the depth < 9,
    else it choices a random coordinate.
    """
    depth = len(empty_cells(board))
    if depth == 0 or game_over(board):
        return

    print_game_state(board)
    print(f'AI turn [{c_choice}]')


    if depth == 9:
        x = choice([0, 1, 2])
        y = choice([0, 1, 2])
    else:
        move = minimax(board, depth, COMP)
        x, y = move[0], move[1]

    set_move(x, y, COMP)
    time.sleep(1)


def random_turn(c_choice, r_choice):

    depth = len(empty_cells(board))
    if depth == 0 or game_over(board):
        return

    # Dictionary of valid moves
    move = -1
    moves = {
        1: [0, 0], 2: [0, 1], 3: [0, 2],
        4: [1, 0], 5: [1, 1], 6: [1, 2],
        7: [2, 0], 8: [2, 1], 9: [2, 2],
    }

    print_game_state(board)
    print(f'Random turn [{r_choice}]')

    move = random.randint(1, 9)
    coord = moves[move]
    can_move = set_move(coord[0], coord[1], RANDOM)
    while not can_move:
        move = random.randint(1, 9)
        coord = moves[move]
        can_move = set_move(coord[0], coord[1], RANDOM)


oWins= 0 
xWins= 0
draws= 0
def main():
    
    """
    Main function that calls all functions
    """   
    r_choice = 'O'  # X or O
    c_choice = 'X'  # X or O
    first = random.randint(0, 1)
    if first == 0:
        print('X starts first')
    elif first == 1:
        print('O starts first')


    # Main loop of this game
    while len(empty_cells(board)) > 0 and not game_over(board):
        if first == 0:
            ai_turn(c_choice, r_choice)
            first = -1
  
        random_turn(c_choice, r_choice)
        ai_turn(c_choice, r_choice)

    # Game over message
    if move_was_winning_move(board, RANDOM):
        print_game_state(board)
        print('O WIN!')
        global oWins
        oWins += 1
    elif move_was_winning_move(board, COMP):        
        print_game_state(board)
        print('X Win!')
        global xWins
        xWins += 1
    else:  
        print_game_state(board)
        print('DRAW!')
        global draws 
        draws += 1
        



if __name__ == '__main__':

    for i in range(0,100):  
        board = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        ]
        main()
    x = np.arange(3)
    plt.bar(x, height=[oWins,draws,xWins])
    plt.xticks(x, ['Player O','Draw','Player X'])
    plt.hist(x, bin = 5)